package com.example.micalculadora;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class CalculatorActivity extends AppCompatActivity {

    TextView textView;
    Button button1, button2, button3, button4, button5, button6, button7, button8, button9, button0, buttonadd, buttonsub, buttonmul, buttondiv, buttoneq, buttoner;
    boolean add, sub, mul, div;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        button1 = findViewById(R.id.button_1);
        button2 = findViewById(R.id.button_2);
        button3 = findViewById(R.id.button_3);
        button4 = findViewById(R.id.button_4);
        button5 = findViewById(R.id.button_5);
        button6 = findViewById(R.id.button_6);
        button7 = findViewById(R.id.button_7);
        button8 = findViewById(R.id.button_8);
        button9 = findViewById(R.id.button_9);
        button0 = findViewById(R.id.button_0);
        buttonadd = findViewById(R.id.button_addition);
        buttonsub = findViewById(R.id.button_subtraction);
        buttonmul = findViewById(R.id.button_multiply);
        buttondiv = findViewById(R.id.button_divide);
        buttoneq = findViewById(R.id.button_equals);
        buttoner = findViewById(R.id.button_erase);
        textView = findViewById(R.id.text_view_number);

        Resources res = getResources();

        button1.setOnClickListener(v -> textView.append(res.getString(R.string.button_1)));
        button2.setOnClickListener(v -> textView.append(res.getString(R.string.button_2)));
        button3.setOnClickListener(v -> textView.append(res.getString(R.string.button_3)));
        button4.setOnClickListener(v -> textView.append(res.getString(R.string.button_4)));
        button5.setOnClickListener(v -> textView.append(res.getString(R.string.button_5)));
        button6.setOnClickListener(v -> textView.append(res.getString(R.string.button_6)));
        button7.setOnClickListener(v -> textView.append(res.getString(R.string.button_7)));
        button8.setOnClickListener(v -> textView.append(res.getString(R.string.button_8)));
        button8.setOnClickListener(v -> textView.append(res.getString(R.string.button_8)));
        button9.setOnClickListener(v -> textView.append(res.getString(R.string.button_9)));
        button0.setOnClickListener(v -> textView.append(res.getString(R.string.button_0)));
        buttoner.setOnClickListener(v -> {
            String newText = textView.getText().toString();
            if(!TextUtils.isEmpty(newText)) {
                newText  = newText.substring(0, newText.length() - 1);
                textView.setText(newText);
            }
        });
        buttonadd.setOnClickListener(v -> {
            String lastNumber = textView.getText().toString();
            if(!TextUtils.isEmpty(lastNumber)) {
                int firstNumber = Integer.parseInt(lastNumber);
                add = true;
            }
            textView.append(res.getString(R.string.button_addition));
        });
        buttonsub.setOnClickListener(v -> {
            String lastNumber = textView.getText().toString();
            if(!TextUtils.isEmpty(lastNumber)) {
                int firstNumber = Integer.parseInt(lastNumber);
                sub = true;
            }
            textView.append(res.getString(R.string.button_subtraction));
        });
        buttonmul.setOnClickListener(v -> {
            String lastNumber = textView.getText().toString();
            if(!TextUtils.isEmpty(lastNumber)) {
                int firstNumber = Integer.parseInt(lastNumber);
                sub = true;
            }
            textView.append(res.getString(R.string.button_multiply));
        });
    }
}