package com.example.micalculadora;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    RadioButton radioButtonBasic, radioButtonScientific, radioButtonProgrammer;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButtonBasic = findViewById(R.id.radio_button_basic);
        radioButtonScientific = findViewById(R.id.radio_button_scientific);
        radioButtonProgrammer = findViewById(R.id.radio_button_programmer);
        button = findViewById(R.id.button_calculator);

        button.setOnClickListener(v -> {
            if (!radioButtonBasic.isChecked() && !radioButtonScientific.isChecked() && !radioButtonProgrammer.isChecked()) {
                Snackbar.make(v, R.string.snack_bar_msg,Snackbar.LENGTH_LONG).show();
                return;
            }

            if (radioButtonBasic.isChecked()) {
                Intent intent = new Intent(MainActivity.this, CalculatorActivity.class);
                intent.putExtra("BASIC", true);
                startActivity(intent);
            }
            if (radioButtonScientific.isChecked()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.dialog_on_work_title);
                builder.setCancelable(false);
                builder.setMessage(R.string.on_work_msg);
                builder.setPositiveButton("Si", (dialog, which) -> {
                    //
                });
                builder.setNegativeButton("NO", (dialog, which) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                builder.create().show();
            }
            if (radioButtonProgrammer.isChecked()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.dialog_on_work_title);
                builder.setCancelable(false);
                builder.setMessage(R.string.on_work_msg);
                builder.setPositiveButton("Si", (dialog, which) -> {
                    //
                });
                builder.setNegativeButton("NO", (dialog, which) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                builder.create().show();
            }
        });
    }
}